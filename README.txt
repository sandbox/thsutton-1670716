UC Webpay
=========

This module implements an Ubercart payment method for the St. George Bank
Webpay credit-card processing API. It requires:

- Drupal 7.x
- Ubercart 3.x
- Webpay PHP extension

PCI DSS Compliance
------------------

No effort has been made to evaluate this module for compliance with Payment
Card Industry Data Security Standards. If you are required to comply with the
PCI DSS you should not use this module!

PHP Extension
-------------

The Webpay PHP5 extension must be installed and enabled for this module to
work. The extension *will not* by dynamically loaded.

For more information, and to download the extension itself, see the [St.
George IPG][1] web-site.

[1]: https://www.ipg.stgeorge.com.au/